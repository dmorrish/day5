#include <stdio.h>

#define MAXCHAR 20
#define MAXSEATS 1000

int main(int argc, char **argv)
{
	FILE *fp;
    char str[MAXCHAR];
    char* filename = "C:\\Users\\deryk\\Desktop\\adventOfCode2020\\day5.txt";
    unsigned int seatIDs[MAXSEATS];
    int seatCounter = 0;
    
    fp = fopen(filename, "r");
    
    if(fp == NULL)
    {
        printf("File not found. Go back to iRacing.");
        return 1;
    }
    
    unsigned int maxSeatID = 0;
    
    while(fgets(str, MAXCHAR, fp) != NULL)
    {
        if(str[0]=='\n' || str[0] == '\0'|| str[0] == '\r')
            break;
            
        unsigned int seatID = 0;
        
        // The boarding pass is simply a binary number where 'B' and 'R'
        // represent 1s and 'F' and 'L' represent 0s. Shift the bits into
        // and unsigned int.
        for(int i =0; str[i]!='\n'; i++)
        {
            seatID = seatID<<1;
            
            if(str[i] == 'B' || str[i] == 'R')
                seatID |= 0b1;
        }
        
        seatIDs[seatCounter] = seatID;
        seatCounter++;
        
        if(seatID > maxSeatID)
            maxSeatID = seatID;
    }
    
    printf("Max seatID is: %u\n", maxSeatID);
    
    // Scan the array if IDs. If I can't find the seatID one value above but I CAN
    // find the ID two values above, then my ID is the missing ID one value above.
    for(int i=0; i<seatCounter; i++)
    {
        int seatAfterFound = 0;
        int seatOfInterestFound = 0;
        
        for(int j=0; j<seatCounter; j++)
        {
            if(seatIDs[j] == seatIDs[i]+1)
                seatOfInterestFound = 1;
            else if(seatIDs[j] == seatIDs[i]+2)
                seatAfterFound = 1;
        }
        
        if(seatAfterFound && !seatOfInterestFound)
            printf("Your seat is: %u. Now you can watch the F2 feature race. Hooray!\n", seatIDs[i]+1);
    }
    
    fclose(fp);
	return 0;
}
